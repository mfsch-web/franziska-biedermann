---
title: Über mich
image: portrait.jpg
sidebar: |
  Franziska Biedermann Gutknecht, lic.phil. I

  Eidgenössisch anerkannte Psychotherapeutin

  Praxisbewilligung des Kantons Basel-Stadt

  ---

  Sprachen: Je parle couramment le français et suis en mesure d’assumer un accompagnement dans cette langue, voire un travail thérapeutique.

  More than school English

  Privates: Jahrgang 60, 1 Ehemann, 1 Sohn, 1 Tochter, 1 Lieblingstanz (derjenige aus Buenos Aires)
---
Ausbildung  
Studium der Sozialarbeit und Psychologie, Abschluss 1988, Universität Fribourg (Schweiz), Weiterbildung in Integrativer Psychotherapie, Abschluss 1998, Fritz Perls Institut FPI (Deutschland)

Professionelles  
Meine berufliche Laufbahn führte mich zunächst an die Universität Basel als Mitverfasserin der Studie über die Armut in Basel-Stadt. Danach war ich 15 Jahre als Sozialarbeiterin und Psychotherapeutin bei der Familien-, Paar- und Erziehungsberatung Basel engagiert, anschliessend 8 Jahre als Case Managerin in einem privaten Unternehmen.


::: columns
::: column
## Berufsfeld heute
- Psychotherapeutin in eigener Praxis
- Supervisorin an der FHNW
- Coach look@work des GGG Wegweiser Basel
:::
::: column
## Verbandsmitgliedschaft
- Schweizerischer Berufsverband für Angewandte Psychologie SBAP
- Schweizerischer Verein für Gestalttherapie und Integrative Therapie SVG
- Mitglied der Charta für Psychotherapie
:::
::: column
## Konditionen
- Die Psychotherapie ist durch die Grundversicherung Ihrer Krankenkasse gedeckt.
- Keine Verrechnung bei Absage 24 Stunden vor Termin
:::
:::
