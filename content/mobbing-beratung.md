---
title: Mobbingberatung
subnav: true
---
Sie erleben Mobbing am Arbeitsplatz? Sie suchen nach Lösungen? Ich biete Ihnen Unterstützung an. Durch meine langjährige Berufserfahrung im Umgang mit Konflikten am Arbeitsplatz sind mir unzählige Formen von Entwertung, Ausgrenzung und Schikane am Arbeitsplatz begegnet.

Mobbing hat Folgen  
Die Folgen von erlittenem Mobbing sind vielfältig und zumeist belastend. Hier einige der bekannten Symptome: Erschöpfung, Depression, Selbstzweifel, Schlafstörungen, sozialer Rückzug, Konzentrationsschwierigkeiten, Angstzustände, körperliche Symptome, Auswirkungen auf das private Umfeld.

Angebot  
Im Erstgespräch klären wir Ihr Anliegen und auf Wunsch begleite ich Sie danach aufmerksam und achtsam, bis sich Ihre Situation beruhigt und verbessert hat.

::: columns
::: column
## Zu meiner Person
- Eidg. anerkannte Psychotherapeutin in eigener Praxis
- Langjährige Berufserfahrung im Umgang mit Konflikten am Arbeitsplatz und Mobbing
- Gute Kenntnisse des Arbeits- und Sozialversicherungsrechts
- [mehr zu meiner Person](/ueber-mich)
:::
::: column
## Kontakt
- [Kontaktangaben](/kontakt)
:::
::: column
## Hilfreiche Links
- [Informationen des Staatssekretariats für Wirtschaft SECO](https://www.seco.admin.ch/seco/de/home/Arbeit/Arbeitsbedingungen/gesundheitsschutz-am-arbeitsplatz/Psychosoziale-Risiken-am-Arbeitsplatz/Mobbing.html)
:::
:::
