---
title: Psychotherapie
subnav: true
---
Das psychotherapeutische Angebot richtet sich an neugierige, erwachsene Menschen, die mehr über sich erfahren möchten. Ganz unabhängig davon, ob sie sich in einer schwierigen Lebenssituation befinden oder nicht.

Im engagierten Gespräch werden neue Erkenntnisse über bisherige Erfahrungen und Empfindungen gewonnen, wird das Verstehen biographischer und sozialer Zusammenhänge geschärft. Meine Haltung? Zugewandt, interessiert, verständnis- und respektvoll. Grossen Wert lege ich auf eine ganzheitliche Sichtweise (systemischer Blickwinkel).

Ziel jeder Therapie: Versteckte, ignorierte und entsprechend brachliegende Ressourcen finden und fördern sowie neue und kreative Verhaltensweisen bei belastenden Situationen entwickeln. In einem ersten Gespräch klären wir gemeinsam die individuellen Zielsetzungen der Psychotherapie.

Psychotherapeutische Unterstützung ist insbesondere bei Beziehungskonflikten oder Trennung/Scheidung, bei lang andauernder Krankheit und Depression, bei Selbstwertproblemen, Ängsten und Verlusten sowie bei Konflikten am Arbeitsplatz hilfreich. Der psychotherapeutische Ansatz eignet sich gut zur Vertiefung weiterer Lebensthemen – ob mit oder ohne Krise.
