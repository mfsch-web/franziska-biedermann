---
title: Spezialgebiete
subnav: true
---
Langjährige Berufserfahrung mit Menschen:

- die sich in einer Trennung/Scheidung befinden
- die Beziehungskonflikte erleben
- die unter einer Mobbingsituation leiden
- die Konflkte am Arbeitsplatz haben,
- die in Folge Krankheit/Unfall eine eingeschränkte Arbeitsfähigkeit haben
- die im Sozialbereich tätig sind und gerne das Angebot der Supervision nutzen

Auf Anfrage übernehme ich für Kostenträger Case Management Mandate für Menschen mit gesundheitlichen Einschränkungen. Mein Plus: Viel know-how in der Zusammenarbeit mit sämtlichen involvierten Sozialversicherungen.
