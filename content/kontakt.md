---
title: Kontakt
style: contact
name: Franziska Biedermann
description: Eidgenössisch anerkannte Psychotherapeutin
street: Im Holeeletten 19
zipcode: 4054
town: Basel
phone: 079 102 38 28
directions:
map-image: plan-klein.png
map-link: https://www.google.com/maps/search/?api=1&query=Im%20Holeeletten%2019%2C%204054%20Basel&query_place_id=ChIJQ7ODG_u4kUcRdhTy_cyawRM
---
