---
title: Supervision
subnav: true
---
Das Supervisionsangebot richtet sich an berufstätige SozialarbeiterInnen, an Case-ManagerInnen und andere Fachleute im Sozialbereich. Supervision dient der Qualitätssicherung der eigenen Arbeit und bietet Unterstützung bei belastenden Situationen im Berufsalltag.

Seit 2008 bin ich unter anderem als Supervisorin tätig – zum Beispiel an der Fachhochschule Nordwestschweiz FHNW. Ich biete so genannte Fallsupervison an, für Einzelpersonen oder in Gruppen.

Supervision unterstützt Sie in Ihrem anspruchsvollen Berufsalltag und bietet Ihnen:

- Fachlichen und persönlichen Input
- Reflexion sowie Entwicklung geeigneter Interventionen
- Analyse komplexer Systemzusammenhänge
- Hinweise und Fakten für die Beratung der KlientInnen sowie für den kompetenten Umgang mit anderen Beteiligten im System (z.B. Angehörige, Behörden, Arbeitgebende bzw. Vorgesetzte, Versicherungen, ÄrztInnen und Kliniken)
- Wahrnehmen, Verstehen und Reflektieren eigener Emotionen
- Weiterentwicklung der Beratungskompetenz
- Prävention eigener Überbeanspruchung (Psychohygiene)
