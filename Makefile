.DEFAULT_GOAL := site
.PHONY: site clean

SITE = public
TOOLS = tools

PANDOC_VERSION = 2.11.1.1
pandoc = $(TOOLS)/pandoc-$(PANDOC_VERSION)/bin/pandoc
pandoc_options = --from=markdown-auto_identifiers-smart --wrap=preserve
SASS_VERSION = 1.29.0
sass = $(TOOLS)/dart-sass/sass

static = $(patsubst static/%,/%,$(wildcard static/*))
images = $(patsubst images/%,/img/%,$(wildcard images/*))
scripts = $(patsubst scripts/%,/js/%,$(wildcard scripts/*.js) $(wildcard scripts/vendor/*.js))
content = $(patsubst content/%.md,/%,$(wildcard content/*.md))
$(info content: $(content))

paths = $(addsuffix .html,$(content)) \
    /css/main.css \
    $(scripts) \
    $(images) \
    $(static)

site: $(addprefix $(SITE),$(paths))

clean:
	rm -rf $(SITE) $(TOOLS)

$(pandoc):
	mkdir -p $(TOOLS)
	wget -O $(TOOLS)/pandoc.tar.gz https://github.com/jgm/pandoc/releases/download/$(PANDOC_VERSION)/pandoc-$(PANDOC_VERSION)-linux-amd64.tar.gz
	tar -xzf $(TOOLS)/pandoc.tar.gz -C $(TOOLS)

$(sass):
	mkdir -p $(TOOLS)
	wget -O $(TOOLS)/dart-sass.tar.gz https://github.com/sass/dart-sass/releases/download/$(SASS_VERSION)/dart-sass-$(SASS_VERSION)-linux-x64.tar.gz
	tar -xzf $(TOOLS)/dart-sass.tar.gz -C $(TOOLS)

$(addprefix $(SITE),$(static)): $(SITE)/%: static/%
	mkdir -p $(@D)
	cp $< $@

$(SITE)/css/main.css: styles/main.scss $(wildcard styles/_*.scss) | $(sass)
	mkdir -p $(@D)
	$(sass) $< $@

$(SITE)/img/%: images/%
	mkdir -p $(@D)
	cp $< $@

$(SITE)/js/%: scripts/%
	mkdir -p $(@D)
	cp $< $@

$(SITE)/kontakt.html: templates/contact.html

# html files with their own templates (cv, kontakt)
$(SITE)/%.html: content/%.md templates/default.html templates/header.html templates/footer.html | $(pandoc)
	mkdir -p $(@D)
	$(pandoc) $(pandoc_options) --template=templates/default.html $(subst /, --variable=nav-,/$*) --output=$@ $<

